FROM node:13-alpine

COPY app/ /usr/app/

WORKDIR /usr/app

RUN npm install
RUN npm run test
CMD ["node", "server.js"]
